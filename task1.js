// load libraries
const jsonfile = require("jsonfile");
const randomstring = require("randomstring");

// input file
const inputFile = "input2.json";

// output
const outputFile = "output2.json";
const output = {};
output.emails = [];

// load input file
jsonfile.readFile(inputFile)
  .then(obj => {
      const emailList = obj.names.map((name) => {
        // reverse single name
        let reverseName = name.split('').reverse().join('');
        // generate random characters
        let randomChars = randomstring.generate(5);
        // append characters and @gmail.com to the reversed name
        reverseName += randomChars + '@gmail.com';
        return reverseName;
      });

      output.emails = emailList;
      // write email list to the output file
      jsonfile.writeFile(outputFile, output, function (err) {
        if (err) console.error(err)
        console.log('All done!');
      })
  })
  .catch(error => console.error(error));